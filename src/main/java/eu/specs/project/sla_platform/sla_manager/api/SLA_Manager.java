package eu.specs.project.sla_platform.sla_manager.api;

import java.util.List;

import eu.specs.project.sla_platform.sla_manager.api.entities.Alert;
import eu.specs.project.sla_platform.sla_manager.api.entities.Annotation;
import eu.specs.project.sla_platform.sla_manager.api.entities.Pair;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Query;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_STATE;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Document;
import eu.specs.project.sla_platform.sla_manager.api.entities.Violation;
import eu.specs.project.sla_platform.sla_manager.internal.Lock;
import eu.specs.project.sla_platform.sla_manager.internal.SLA_Identifier;

/**
 *  The SLA_Manager permits to manage the persistence and the life-cycle of SLAs.
 *  An actual instance can be obtained by using the {@link  SLA_Manager_Factory}.
 *  
 * 
 * @author Mauro Turtur SPECS - CeRICT
 * 
 */
public interface SLA_Manager {

	/**
	 * Create and persist a SLA into the SLA Platform.
	 * The SLA has to be not null, well formed and valid (see {@link SLA_Document}), otherwise an {@link IllegalArgumentException} will be thrown.
	 * The SLA will be put in the {@link SLA_STATE#PENDING} state. 
	 * If no errors occur the state automatically evolves into {@link SLA_STATE#NEGOTIATING}, in {@link SLA_STATE#REJECTED} otherwise. 
	 * TODO controllare ->  In the latter case, error details can be found on an attached {@link Annotation}, as well in the application log.
	 * 
	 * @param sla - A valid and well formed SLA to persist.
	 * @return an opaque object that identifies the created SLA, intended to be used to perform further operation on it.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 */
	public SLA_Identifier create(SLA_Document sla) throws IllegalArgumentException;
	
	
	/**
	 * Retrieve a SLA from the persistence.
	 * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @return the persisted SLA
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 */
	public SLA_Document retrieve(SLA_Identifier id) throws IllegalArgumentException;
	

	
	/**
	 * Retrieve a SLA from the persistence and lock it in order to perform an update.
	 * The lock can be acquired only in  {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING state}, otherwise
	 * an {@link IllegalStateException} will be thrown.
	 * If the lock cannot be acquired (i.e., the SLA is already locked), the returned Lock Object will be null.
	 * The Lock is an opaque object that must be used in order to perform further operations on the (locked) SLA.
	 * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @return a {@link Pair}  Object, which {@link Pair#obj1} is the retrieved SLA and {@link Pair#obj2} is the Lock. {@link Pair#obj2}can be null if Lock is not acquired.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 * @throws IllegalStateException in case of SLA is not in  {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING} state. 
	 */
	Pair<SLA_Document, Lock> retrieveAndLock(SLA_Identifier id) throws IllegalArgumentException, IllegalStateException;
	
	
	
	/**
	 * 
	 * Evolve the SLA status from {@link SLA_STATE#SIGNED} to {@link SLA_STATE#OBSERVED}. 
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 * @throws IllegalStateException in case of SLA is not in  {@link SLA_STATE#SIGNED}. 
	 */
	public void observe(SLA_Identifier id) throws IllegalArgumentException, IllegalStateException;
	
	
	/**
	 * 
	 * Permit to send a termination request to the SLA.
	 * It causes the status to evolve to {@link SLA_STATE#TERMINATING}.
	 * If no internal error occur, the Status automatically evolves to {@link SLA_STATE#TERMINATED}. 
	 * TODO controllare -> If the status is Locked, the termination request will be delayed until the lock is released. 
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 * @throws IllegalStateException in case of SLA is not in {@link SLA_STATE#OBSERVED} or {@link SLA_STATE#REDRESSING} or {@link SLA_STATE#REMEDIATING} or {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING}. 
	 */
	public void terminate(SLA_Identifier id) throws IllegalArgumentException, IllegalStateException;
	
	
	/**
	 * 
	 * Permit to complete a SLA in {@link SLA_STATE#OBSERVED} state.
	 * TODO lock?!
	 * @param id - a valid and not null SLA identifier.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 * @throws IllegalStateException in case of SLA is not in {@link SLA_STATE#OBSERVED}. 
	 */
	public void complete(SLA_Identifier id) throws IllegalArgumentException, IllegalStateException;
	
	
	/**
	 * 
	 * Retrieve the SLA current state.
	 *
	 * @param id - a valid and not null SLA identifier.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
	 */
	public SLA_STATE getState(SLA_Identifier id)  throws IllegalArgumentException;
	

	/**
	 * Permit to evolve the SLA status from  {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING} to {@link SLA_STATE#SIGNED}. 
	 * The sla argument is optional and can be null. Otherwise, if a SLA has been specified, the operation can be invoked only after a proper Lock has been acquired.
	 *  After operation successful execution, the SLA will be unlocked.
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @param sla - the new SLA to be signed. can be null.
	 * @param lock - the acquired lock, is mandatory if a sla is specified, it should be null otherwise.
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed, or a SLA has been specified but lock is null or not valid.
	 * @throws IllegalStateException in case of SLA is not in {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING}; the SLA was not locked and a sla argument has been specified.
	 */
	public void sign(SLA_Identifier id, SLA_Document sla,Lock lock ) throws IllegalArgumentException, IllegalStateException;
	
	
	/**
	 * The operation permits to  modify a SLA document. 
	 * The operation will succeed only in  {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING} state and requires a Lock acquired by {@link SLA_Manager#retrieveAndLock(SLA_Identifier)}.
	 *
	 * @param id - a valid and not null SLA identifier.
	 * @param sla - the new SLA.
	 * @param lock - the acquired lock, cannot be null.
	 * @throws IllegalArgumentException in case of id and/or SLA and/or lock arguments are null.
	 * @throws IllegalStateException in case of SLA is not in {@link SLA_STATE#NEGOTIATING} or {@link SLA_STATE#RENEGOTIATING}.
	 */
	public void update(SLA_Identifier id, SLA_Document sla, Lock lock) throws IllegalArgumentException, IllegalStateException;
	
	
	//TODO
	public void signalAlert(SLA_Identifier id,Alert alert) throws IllegalArgumentException, IllegalStateException;
	public void signalViolation(SLA_Identifier id,Violation violation) throws IllegalArgumentException, IllegalStateException;
	public void reNegotiate(SLA_Identifier id);
	public void unlock(SLA_Identifier id);
	
	/**
	 * 
	 * Set an annotation to the given SLA.
	 * An {@link IllegalArgumentException} will be thrown in case of a not valid SLA identifier or a null {@link Annotation}.
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @param annotation - a not null {@link Annotation} Object
	 * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed and/or annotation is null. 
	 */
	public void annotate(SLA_Identifier id,Annotation annotation) throws IllegalArgumentException;
	
	/**
	 * 
	 * Retrieve the {@link List} of {@link Annotation} for a given SLA.
	 * 
	 * 
	 * @param id - a valid and not null SLA identifier.
	 * @return the {@link Annotation} list for the given SLA, may be a 0-length list.
	 * @throws IllegalArgumentException in case of sla argument is null, not valid nor well formed.
	 */
	public List<Annotation> getAnnotations(SLA_Identifier id) throws IllegalArgumentException;
	
	/**
	 * Search SLAs with the specified criteria.
	 * 
	 * @param query - query criteria
	 * @return a list of identifiers, may be empty.
	 */
	public List<SLA_Identifier> search(SLA_Query query);
	
	public void updateAnnotation(SLA_Identifier id);
	public void createReport(SLA_Identifier id);
	public void check(SLA_Identifier id);
	
	

}
