package eu.specs.project.sla_platform.sla_manager.internal;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.specs.project.sla_platform.sla_manager.api.entities.Annotation;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_STATE;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Document;

@Entity
public class SLA {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false) 
	int id;
	public SLA(){
		created = updated = new Date();
	}
	///
	@Temporal(TemporalType.DATE)
	@Column(nullable = false) 
	Date created;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false) 
	Date updated;
	
	@Column(nullable = false) 
	SLA_STATE state;
	
	@Column(nullable = false) 
	SLA_Document doc;

	@Basic(fetch=FetchType.LAZY)
	List<Annotation> annotations = new ArrayList<Annotation>();
	
	@Embedded
	Lock lock;

//		
	 


}
