package eu.specs.project.sla_platform.sla_manager.internal;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Lock {
	
	private UUID lockid;
	
	@Temporal(TemporalType.DATE)
	private Date timestamp;
	
	Lock (){
		lockid=UUID.randomUUID();
		timestamp=(new Date());
	}
	
	 
	@Override
	public boolean equals(Object obj) {
		
		 if (this == obj) return true;
		    if (obj == null || getClass() != obj.getClass()) return false;

		    Lock lock = (Lock) obj;

		    if (!lockid.equals(lock.lockid)) 
		    	return false;
		    
		    return true;
	}


	public Date getTimestamp() {
		return timestamp;
	}

}
