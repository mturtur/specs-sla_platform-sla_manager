package eu.specs.project.sla_platform.sla_manager.internal;

import java.util.List;

import eu.specs.project.sla_platform.sla_manager.api.SLA_Manager;
import eu.specs.project.sla_platform.sla_manager.api.entities.Alert;
import eu.specs.project.sla_platform.sla_manager.api.entities.Annotation;
import eu.specs.project.sla_platform.sla_manager.api.entities.Pair;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Query;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_STATE;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Document;
import eu.specs.project.sla_platform.sla_manager.api.entities.Violation;


/**
 * Internal implementation of {@link SLA_Manager} interface.
 *  This code is not intended to be part of the public API.
 *  
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
abstract class EU_SLA_Manger_AbstractImpl implements SLA_Manager{

	//private final Logger logger = LoggerFactory.getLogger(EU_SLA_Manger_AbstractImpl.class);


	/**
	 * Check SLA state against a vector of valid ones.
	 * If actual state is not found in constraint argument an  {@link IllegalStateException} will be thrown.
	 * 
	 * @param actualState - actual SLA state, not NULL
	 * @param constraints - SLA state vector
	 * @throws IllegalStateException in case actualState is not on constraints vector.
	 */
	private void checkState (SLA_STATE actualState, SLA_STATE[] constraints ) throws IllegalStateException{

		for (SLA_STATE constraint : constraints) {			
			if (actualState == constraint){
				return;
			}
		}
		throw new IllegalStateException("Cannot perform requested operation in" + actualState +"SLA state");
	}

	/**
	 * TODO
	 * 
	 * @param sla
	 * @param newState
	 * @param constraints
	 * @throws IllegalStateException
	 */
	private void changeState(SLA sla, SLA_STATE newState, SLA_STATE[] constraints) throws IllegalStateException{
		checkState(sla.state, constraints);
		sla.state=newState;
		persistenceUpdate(sla);
	}


	/**
	 * 
	 * The method implementation must ensure that the identifier is both not null and valid. 
	 * Otherwise, an {@link IllegalArgumentException} has to be thrown. 
	 * 
	 * @param id - SLA identifier, not null 
	 * @return the queried SLA from persistence
	 * @throws IllegalArgumentException in case id is null or SLA is not found
	 */
	abstract SLA  persistenceGetByID(SLA_Identifier id) throws IllegalArgumentException;

	/**
	 * Create a SLA in persistence.
	 * 
	 * @param sla - the SLA to persist, not null
	 */
	abstract void persistenceCreate(SLA sla);

	/**
	 * update a SLA in persistence.
	 * 
	 * @param sla  - the SLA to update, not null
	 */
	abstract void persistenceUpdate(SLA sla);


	abstract List<SLA_Identifier> persistenceSearch();
	abstract Lock persistenceSetLock(SLA_Identifier id);
	abstract boolean persistenceReleaseLock(SLA_Identifier id, Lock lock);
	///////////////////////////////////////////////////////////////////	



	@Override
	public SLA_Identifier create(SLA_Document sla) {

		if (sla==null)
			throw new IllegalArgumentException("The SLA document cannot be null");

		//create SLA
		SLA internalSLA = new SLA();
		internalSLA.state=SLA_STATE.PENDING;
		internalSLA.doc=sla;
		persistenceCreate(internalSLA);

		//put SLA in negotiating state
		internalSLA.state=SLA_STATE.NEGOTIATING;
		persistenceUpdate(internalSLA);

		return new 	SLA_Identifier(internalSLA.id);
	}


	@Override
	public SLA_Document retrieve(SLA_Identifier id) {

		return  persistenceGetByID(id).doc;	
	}

	@Override
	public Pair<SLA_Document,Lock> retrieveAndLock(SLA_Identifier id) {

		SLA sla = persistenceGetByID(id);
		SLA_STATE [] constraints = {SLA_STATE.NEGOTIATING, SLA_STATE.RENEGOTIATING};
		checkState(sla.state, constraints);

		Lock lock = persistenceSetLock(id);
		return new Pair<SLA_Document,Lock>(sla.doc,lock);	
	}


	@Override
	public void update(SLA_Identifier id, SLA_Document sla, Lock lock) throws IllegalArgumentException ,IllegalStateException { 
		SLA intsla = persistenceGetByID(id);
		SLA_STATE [] constraints = {SLA_STATE.NEGOTIATING, SLA_STATE.RENEGOTIATING};
		checkState(intsla.state, constraints);

		if (sla==null)
			throw new IllegalArgumentException("SLA cannot be null");

		if (lock==null)
			throw new IllegalArgumentException("Lock cannot be null");

		if(!persistenceReleaseLock(id, lock))
			throw new IllegalArgumentException("Lock is not valid");


		intsla.doc=sla;
		persistenceUpdate(intsla);


	}


	@Override
	public void sign(SLA_Identifier id, SLA_Document sla, Lock lock) throws IllegalArgumentException ,IllegalStateException {		

		SLA intsla = persistenceGetByID(id);
		SLA_STATE [] constraints = {SLA_STATE.NEGOTIATING, SLA_STATE.RENEGOTIATING};
		checkState(intsla.state, constraints);		

		if (sla!= null) {
			if (lock == null)
				throw new IllegalArgumentException("If a SLA is specified, lock cannot be null");
			if (intsla.lock!=null)
				throw new IllegalStateException("SLA is not locked");

			if(!persistenceReleaseLock(id, lock))
				throw new IllegalArgumentException("Lock is not valid");

			intsla.doc=sla;

		}

		intsla.state=SLA_STATE.SIGNED;
		persistenceUpdate(intsla);

	}

	@Override
	public void observe(SLA_Identifier id) {

		SLA_STATE [] constraints = {SLA_STATE.SIGNED};
		changeState(persistenceGetByID(id), SLA_STATE.OBSERVED, constraints);		
	}


	@Override
	public void terminate(SLA_Identifier id) {

		SLA sla = persistenceGetByID(id);

		SLA_STATE [] constraints = {SLA_STATE.NEGOTIATING, SLA_STATE.OBSERVED, SLA_STATE.REDRESSING,
				SLA_STATE.RENEGOTIATING, SLA_STATE.REMEDIATING };
		changeState(sla, SLA_STATE.TERMINATING, constraints);	

		//change state

		sla.state=SLA_STATE.TERMINATED;
		persistenceUpdate(sla);
	}

	@Override
	public void complete(SLA_Identifier id) {

		SLA_STATE [] constraints = {SLA_STATE.OBSERVED};
		changeState(persistenceGetByID(id), SLA_STATE.COMPLETED, constraints);

	}


	@Override
	public void signalAlert(SLA_Identifier id, Alert alert) {

		SLA_STATE [] constraints = {SLA_STATE.OBSERVED};
		changeState(persistenceGetByID(id), SLA_STATE.ALERTED, constraints);

	}

	@Override
	public void signalViolation(SLA_Identifier id, Violation violation) {

		SLA_STATE [] constraints = {SLA_STATE.OBSERVED,SLA_STATE.ALERTED};
		changeState(persistenceGetByID(id), SLA_STATE.VIOLATED, constraints);

	}

	@Override
	public void reNegotiate(SLA_Identifier id) {

		SLA_STATE [] constraints = {SLA_STATE.OBSERVED};
		changeState(persistenceGetByID(id), SLA_STATE.VIOLATED, constraints);

	}

	@Override
	public SLA_STATE getState(SLA_Identifier id) {
		return persistenceGetByID(id).state;
	}


	@Override
	public List<SLA_Identifier> search(SLA_Query query) {
	
		//TODO queries
		return persistenceSearch();
	}



	@Override
	public void unlock(SLA_Identifier id) {
		// TODO Auto-generated method stub

	}

	//annotations
	@Override
	public void annotate(SLA_Identifier id, Annotation annotation) {
		if (annotation==null)
			throw new IllegalArgumentException("Annotation cannot be null");

		SLA sla = persistenceGetByID(id);
		sla.annotations.add(annotation);
		persistenceUpdate(sla);
	}

	@Override
	public List<Annotation> getAnnotations(SLA_Identifier id) {

		return persistenceGetByID(id).annotations;	
	}

	@Override
	public void updateAnnotation(SLA_Identifier id) {
		// TODO  
		throw new RuntimeException("not implemented");	
	}

	//////////////////////////////////////////////////////////
	@Override
	public void createReport(SLA_Identifier id) {
		// TODO  
		throw new RuntimeException("not implemented");		
	}

	@Override
	public void check(SLA_Identifier id) {
		// TODO  
		throw new RuntimeException("not implemented");		
	}




}
