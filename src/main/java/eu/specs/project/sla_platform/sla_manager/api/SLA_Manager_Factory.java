package eu.specs.project.sla_platform.sla_manager.api;

import javax.persistence.Persistence;

import eu.specs.project.sla_platform.sla_manager.internal.EU_SLA_Manger_SQL_JPA;


public final class SLA_Manager_Factory {

	public static SLA_Manager get_SLA_Manager_Instance(){
		return new EU_SLA_Manger_SQL_JPA(Persistence.createEntityManagerFactory("t1"));
	}
	
	public static SLA_Manager get_SLA_Manager_Instance(String pu){
		return new EU_SLA_Manger_SQL_JPA(Persistence.createEntityManagerFactory(pu));
	}
	
	
	public static CSP_SLA_Manager get_CSP_SLA_Manager_Instance(){
		//TODO fixme
		return null;
	}

	
	public static void main(String[] args) {
		
		
	}
}

