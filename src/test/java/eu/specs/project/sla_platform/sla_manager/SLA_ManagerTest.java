package eu.specs.project.sla_platform.sla_manager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specs.project.sla_platform.sla_manager.api.SLA_Manager;
import eu.specs.project.sla_platform.sla_manager.api.SLA_Manager_Factory;
import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Document;
import eu.specs.project.sla_platform.sla_manager.internal.SLA_Identifier;

public class SLA_ManagerTest {
	
	private static SLA_Manager manager;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		 manager= SLA_Manager_Factory.get_SLA_Manager_Instance();

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//do nothing
	}

	@Before
	public void setUp() throws Exception {
		//do nothing
	}

	@After
	public void tearDown() throws Exception {
		//do nothing
	}

	@Test
	public final void testCreate() {
		SLA_Identifier id = manager.create(new SLA_Document());
		Assert.assertEquals("1",id.toString()); 
	}

	/*@Test
	public final void testSign() {
		fail("Not yet implemented");  

	
	@Test
	public final void testObserve() {
		fail("Not yet implemented"); 
	}

	@Test
	public final void testTerminate() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testSignalAlert() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testSignalViolation() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testReNegotiate() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testComplete() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testGetStatus() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testAnnotate() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testGetAnnotations() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testUpdateAnnotation() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testCreateReport() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testRetrieve() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testUpdate() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testUnlock() {
		fail("Not yet implemented");  
	}

	@Test
	public final void testCheck() {
		fail("Not yet implemented");   
	}

	@Test
	public final void testSearch() {
		fail("Not yet implemented");  
	}*/

}
