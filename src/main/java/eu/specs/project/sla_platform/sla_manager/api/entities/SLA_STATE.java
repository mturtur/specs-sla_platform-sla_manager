package eu.specs.project.sla_platform.sla_manager.api.entities;

/**
 * The SLA states. See D1.1.1
 * TODO immagine javadoc
 * <img alt="SLA state diagram" src="path/img.jpg">
 * 
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
public enum SLA_STATE {
	
	/**
	 * A request has been submitted, but it has not been yet acquired and processed by the system.
	 */
	PENDING,
	
	
	/**
	 * The request has been rejected without negotiation (unauthorized, server exhausted, target service unavailable, etc.).
	 */
	REJECTED,
	
	
	/**
	 * The SLA is under negotiation with the End-user. Its content may change.
	 */
	NEGOTIATING,
	
	
	/**
	 * The SLA has been signed and is valid. No action is taken yet (for example, the SLA is signed but the starting date is in future). 
	 */
	SIGNED,
	
	
	/**
	 * The SLA has been signed and executed, and is currently under monitoring.
	 */
	OBSERVED,
	
	
	/**
	 * The SLA has expired. This is the SLA final state, in case of the terminating date is in the past.
	 */
	
	COMPLETED,
	
	
	/**
	 * The SLA was in Observed state, and either the customer or the provider of the SLA have made a valid terminating request. 
	 * Terminating state may occur also in case of an agreement is not found with the customer, or as a consequence of a violation or an alert event.
	 */
	TERMINATING,
	
	
	/**
	 * The SLA has been terminated before terminating date.
	 */
	TERMINATED,
	
	
	/**
	 * An alert has been raised related to an SLA that is currently in the Observed state (no violations). 
	 * A violation may occur soon. 
	 */ 
	ALERTED,
	 
	
	/**
	  * A violation has been detected for an SLA that was in the Observed state.
	  */  
	VIOLATED,
	 
	
	/**
	  * Actions (e.g., service reconfiguration) are being taken to handle an alert raised for an SLA, in order to avoid future violations. 
	  * The process may require a re-negotiation phase.
	  */
	  REDRESSING,
	 
	  
	  /**
	   *  Actions are being taken to react to a violation. The process may require a re-negotiation phase.
	   */
	  REMEDIATING,
	  
	  
	  /**
	   * An alerted or violated SLA is being changed under a process of re-negotiation between the SLA customer and provider.
	   */
	  RENEGOTIATING	
 
}
