package eu.specs.project.sla_platform.sla_manager.internal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * JPA SQL implementation.
 * This code is not intended to be part of the public API.
 * 
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
public final class EU_SLA_Manger_SQL_JPA extends EU_SLA_Manger_AbstractImpl {

	private EntityManagerFactory emFactory;
	//private final Logger logger = LoggerFactory.getLogger(EU_SLA_Manger_SQL_JPA.class);

	public EU_SLA_Manger_SQL_JPA (EntityManagerFactory factory){
		emFactory = factory;
	}

	
	@Override
	List<SLA_Identifier> persistenceSearch(){
		
		EntityManager em =  emFactory.createEntityManager();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SLA> cq = cb.createQuery(SLA.class);
        Root<SLA> rootEntry = cq.from(SLA.class);
        CriteriaQuery<SLA> all = cq.select(rootEntry);
        TypedQuery<SLA> allQuery = em.createQuery(all);
           
        List<SLA_Identifier> result = new ArrayList<SLA_Identifier>();
        
        for (SLA sla :  allQuery.getResultList()){
        	result.add(new SLA_Identifier(sla.id));
        }
        
        em.close();
        return result;
		
	}

	@Override
	void persistenceCreate(SLA sla) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(sla);
		t.commit();
		em.close();
	}

	@Override
	SLA persistenceGetByID(SLA_Identifier id) {
		if (id==null)
			throw new IllegalArgumentException("SLA identifier cannot be null");
		int i_d = id.id;
		SLA sla = emFactory.createEntityManager().find(SLA.class,i_d);
		
		if (sla==null)
			throw new IllegalArgumentException("SLA identifier is not valid");
		
		return sla;
	}

	@Override
	void persistenceUpdate(SLA sla) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		sla.updated=new Date();
		em.merge(sla);
		t.commit();
		em.close();
	}


	@Override
	Lock persistenceSetLock(SLA_Identifier id) {

		Lock lock = new Lock();
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		SLA sla = em.find(SLA.class, id.id, LockModeType.PESSIMISTIC_WRITE);
		if(sla.lock==null){
			sla.lock=lock;
			sla.updated=new Date();
		} else 
			lock=null;

		t.commit();
		em.close();

		return lock;
	}


	@Override
	boolean persistenceReleaseLock(SLA_Identifier id, Lock lock) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		boolean ok=false;

		t.begin();
		SLA sla = em.find(SLA.class, id.id, LockModeType.PESSIMISTIC_WRITE);

		if(lock.equals(sla.lock)){
			sla.lock=null;
			ok=true;
		}

		t.commit();
		em.close();

		return ok;
	}

}
