package eu.specs.project.sla_platform.sla_manager.internal;

public final class SLA_Identifier {
	
	int id;
	
	public SLA_Identifier(int id){
		this.id=id;
	}

	@Override
	public String toString() {
		return id+"";
	}

}
